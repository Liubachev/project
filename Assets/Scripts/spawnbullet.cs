﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
 



public class spawnbullet : MonoBehaviour
   
{

    public Camera cam; //камера на сцене 
    public static Vector3 direction; //направление полета ядра 
    public GameObject bullet; //само ядро 
   

    
    void FixedUpdate()
    {
        if (Spawntarget.bullets >= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                direction = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 3));
                Instantiate(bullet);
                Spawntarget.bullets -= 1;
            }
        }
        else { SceneManager.LoadScene(SceneManager.GetActiveScene().name);} //перезагружаем сцену при нуле снарядов 
    }
}
